﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ProgramowanieWPF.ViewModel.Commands
{
    class DeleteCommand : ICommand
    {
        public BookVM vm { get; set; }
        public DeleteCommand(BookVM VM)
        {
            vm = VM;
        }
        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            vm.Delete();
        }
    }
}
