﻿using ProgramowanieWPF.Model;
using ProgramowanieWPF.ViewModel.Commands;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;

namespace ProgramowanieWPF.ViewModel
{
    class BookVM : INotifyPropertyChanged
    {
        private ObservableCollection<Book> books;

        public ObservableCollection<Book> Books
        {
            get { return books; }
            set { books = value; }
        }
        private Book selected;



        public Book Selected
        {
            get { return selected; }
            set
            {
                selected = value;
                OnPropertyChanged("Selected");
                if (Selected != null)
                {
                    IsSelected = true;
                }
                else
                {
                    IsSelected = false;
                }
                
            }
        }

        private bool isSelected = false;

        public bool IsSelected
        {
            get { return isSelected; }
            set
            {
                isSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }

        private string author;

        public string Author
        {
            get { return author; }
            set
            {
                author = value;
                OnPropertyChanged(nameof(Author));
                if (Pages != null && Title != null && Author != null)
                {
                    CanCreate = true;
                }
            }
        }
        private string title;

        public string Title
        {
            get { return title; }
            set
            {
                title = value;
                OnPropertyChanged(nameof(Title));
                if (Pages != null && Title != null && Author != null)
                {
                    CanCreate = true;
                }
            }
        }

        private int? page = null;

        public int? Pages
        {
            get { return page; }
            set
            {
                page = value;
                OnPropertyChanged(nameof(Pages));
                if (Pages != null && Title != null && Author != null)
                {
                    CanCreate = true;
                }
            }
        }

        private bool canCreate = false;

        public bool CanCreate
        {
            get { return canCreate; }
            set
            {
                canCreate = value;
                OnPropertyChanged(nameof(CanCreate));
            }
        }

        public DeleteCommand deleteCommand { get; set; }
        public CreateCommand createCommand { get; set; }
        public int minId { get; set; }

        public BookVM()
        {
            deleteCommand = new DeleteCommand(this);
            createCommand = new CreateCommand(this);

            Books = new ObservableCollection<Book>
            {
                new Book{ Id = 1, Author = "Jane Austin", Title="Perswazje", AmountOfPages = 223},
                new Book{ Id = 2, Author = "Emily Bronte", Title="Wichrowe wzgórza", AmountOfPages = 423},
                new Book{ Id = 3, Author = "Lucy Maud Montgomery", Title="Ania z Zielonego Wzgórza", AmountOfPages = 501},
                new Book{ Id = 4, Author = "Lew Tołstoj", Title="Anna Karenina", AmountOfPages = 897},
                new Book{ Id = 5, Author = "Ignacy Kraszewski", Title="Chata za wsią", AmountOfPages = 567},
                new Book{ Id = 6, Author = "Władysław Reymont", Title="Chłopi", AmountOfPages = 346},
                new Book{ Id = 7, Author = "Maria Kuncewiczowa", Title="Cudzoziemka", AmountOfPages = 567},
                new Book{ Id = 8, Author = "Boccaccio", Title="Dekameron", AmountOfPages = 344},
                new Book{ Id = 9, Author = "Bolesław Prus", Title="Emancypatki", AmountOfPages = 778},
                new Book{ Id = 10, Author = "Victor Hugo", Title="Nędznicy", AmountOfPages = 566},
                new Book{ Id = 11, Author = "Maria Dąbrowska", Title="Noce i dnie", AmountOfPages = 678},
                new Book{ Id = 12, Author = "Franz Kafka", Title="Proces", AmountOfPages = 972},
                new Book{ Id = 13, Author = "Margarett Mitchell", Title="Przeminęło z wiatrem", AmountOfPages = 235},
                new Book{ Id = 14, Author = "Gustav Flaubert", Title="Pani Bovary", AmountOfPages = 478},
            };
            minId = Books.Count() + 1;
        }

        public void Delete()
        {
            Books.Remove(Books.Where(t => t.Id == Selected.Id).SingleOrDefault());
        }
        public void Create()
        {
            var nextId = Books.Count() + 1;
            if (nextId < minId)
            {
                nextId = minId;
            }
            Books.Add(new Book{Id = nextId, Author = Author, Title = Title, AmountOfPages = Pages });
            Author = null;
            Title = null;
            Pages = null;
            CanCreate = false;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
